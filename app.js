
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;


app.post('/', (req, res) =>
    res.send('Welcome To Core Mock Service')
)

app.post('/auth-integration-service/v1/auth-services/debit-card/verify', (req, res) =>
    res.send('{"customerId":"2000248"}')
)

app.post('/auth-integration-service/v1/auth-services/otp/generate' , (req, res) =>
    res.send('{"otp":"12323", "utr":"23423"}')
)

app.post('/auth-integration-service/v1/auth-services/otp/validate', (req, res) =>
    res.send('{"status":"Success","description":"Mock Server"}')
)

app.listen(port, () => console.log(`Core Mock Service is listening on port ${port}!`))